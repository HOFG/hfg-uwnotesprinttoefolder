﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFG_UWNotesPrintToEfolder.Constants
{
    public static partial class FC
    {
        public static class EmFields
        {
            public const string DOCS = "CX.PNDOCUMENTS";
            public const string Credit = "CX.PNCREDIT";
            public const string Income = "CX.PNINCOME";
            public const string Assets = "CX.PNASSETS";
            public const string Property = "CX.PNPROPERTY";
        }
       
    }
}
