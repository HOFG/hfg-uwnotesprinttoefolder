﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFG_UWNotesPrintToEfolder
{
    internal static class FileLocations
    {
        public static string GetSaveLocation()
        {
            CheckSaveLocationPresent();
            return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\EncompassTemp\Pqf.pdf";
        }

        private static void CheckSaveLocationPresent()
        {
            if (Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\EncompassTemp") == false)
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\EncompassTemp");
            }
        }
    }
}
