﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects;
using System.Windows.Forms;

namespace HFG_UWNotesPrintToEfolder
{
    class HTMLWriter
    {

        public void WriteHTMLValues()
        {
            //StringBuilder sb = new StringBuilder();
            var buildDir = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            var filePath = buildDir + @"\hfgUWNotesPrintToeFolder.html";
            string test = "<!DOCTYPE html>" +
                            "<html lang='en'>" +
                            "<head>" +
                            "<meta charset='UTF-8' />" +
                            "<meta http-equiv='X-UA-Compatible' content='IE=edge' />" +
                            "<meta name='viewport' content='width=device-width, initial-scale=1.0' />" +
                            "<title> </title>" +
                            "</head>" +
                            "<body>" +
                            "<h1 style='border: orange; border-width: 5px; border-style: solid; text-align: center;' >Underwriting Notes</h1>" +
                            "<p style='border: lightblue; border-width: 2px; border-style:solid;'> <h2>" +
                            "Credit:</h2>{Credit}</p>" +
                            "<p style='border: lightblue; border-width: 2px; border-style:solid;'> <h2>" +
                            " Income:</h2>{Income}</p>" +
                            " <p style='border: lightblue; border-width: 2px; border-style:solid;'> <h2>" +
                            "Assets:</h2>{Assets}</p>" +
                            "<p style='border: lightblue; border-width: 2px; border-style:solid;'> <h2>" +
                            "Property:</h2>{Property}</p>" +
                            "<p style='border: lightblue; border-width: 2px; border-style:solid;'> <h2>" +
                            "Other:</h2>{Other}</p>" +
                            "<p style='border: lightblue; border-width: 2px; border-style:solid;'> <h2>" +
                            "Uploaded by:</h2>{Uploaded by} <h2>" +
                            "Uploaded on:</h2>{Uploaded on}</p>" +
                            "<p style='border: lightblue; border-width: 2px; border-style:solid;'></p>" +
                            "</body>" +
                            "</html>";
            //File.ReadAllText(EncompassApplication.CurrentLoan.Fields["CX.UW.NOTES.CREDIT"].Value.ToString());
            //MessageBox.Show("Credit: " + EncompassApplication.CurrentLoan.Fields["CX.UW.NOTES.CREDIT"].Value.ToString());
            test = test.Replace("{Credit}", EncompassApplication.CurrentLoan.Fields["CX.UW.NOTES.CREDIT"].Value.ToString());
            test = test.Replace("{Income}", EncompassApplication.CurrentLoan.Fields["CX.UW.NOTES.INCOME"].Value.ToString());
            test = test.Replace("{Assets}", EncompassApplication.CurrentLoan.Fields["CX.UW.NOTES.ASSETS"].Value.ToString());
            test = test.Replace("{Property}", EncompassApplication.CurrentLoan.Fields["CX.UW.NOTES.PROPERTY"].Value.ToString());
            test = test.Replace("{Other}", EncompassApplication.CurrentLoan.Fields["CX.UW.NOTES.OTHER"].Value.ToString());
            test = test.Replace("{Uploaded by}", EncompassApplication.CurrentUser.FullName);
            test = test.Replace("{Uploaded on}", DateTime.Now.ToString());
            File.WriteAllText(filePath, test);
            EfolderHandler efolderHandler = new EfolderHandler();
            efolderHandler.UploadToEfolder();
            //sb = testv;
        }
    }
}
