﻿using System;
using WinForms = System.Windows.Forms;
using EllieMae.Encompass.Forms;
using HFG_UWNotesPrintToEfolder.Constants;
namespace HFG_UWNotesPrintToEfolder
{
    public class InputForm : Form
    {
        internal Button btnPrintToEfolder = null;
       

        private void Form_load(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public override void CreateControls()
        {
            
            btnPrintToEfolder = FindControl(FC.ControlIds.btnAddToEfolder) as Button;
            btnPrintToEfolder.Click += new EventHandler(btnAddToEfolder_Click);
        }

        private void btnAddToEfolder_Click(object sender, EventArgs e)
        {
            try
            {
                var val = WinForms.MessageBox.Show("Print UW Notes to the eFolder?" + Environment.NewLine + "The document will be tittled 'Underwriting - 1008 - Transmittal Summary'"
                   , "", WinForms.MessageBoxButtons.YesNo, WinForms.MessageBoxIcon.Exclamation);
                if (val == WinForms.DialogResult.Yes)
                {
                    HTMLWriter pdfForm = new HTMLWriter();
                    pdfForm.WriteHTMLValues();
                }
            }
            catch (Exception ex)
            {
                WinForms.MessageBox.Show(ex.Message, "Exception", WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Error);
            }
        }

    }
}
