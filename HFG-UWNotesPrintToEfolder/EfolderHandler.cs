﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;
using EllieMae.Encompass.Collections;

namespace HFG_UWNotesPrintToEfolder
{
    class EfolderHandler
    {
        private TrackedDocument trackedDocument { get; set; }
        private LogEntryList trackedDocuments { get; set; }
        private Attachment attachment { get; set; }

        public EfolderHandler()
        {
            //
        }

        public void UploadToEfolder()
        {
            //MessageBox.Show("efolder section");
            GetTrackedDocument();
            var buildDir = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);           
            var filePath = buildDir + @"\hfgUWNotesPrintToeFolder.html";
            attachment = EncompassApplication.CurrentLoan.Attachments.AddImage(filePath);
            attachment.Title = "Underwriter Notes";
            trackedDocument.Attach(attachment);
        }

        private void GetTrackedDocument()
        {
            trackedDocuments = EncompassApplication.CurrentLoan.Log.TrackedDocuments.GetDocumentsByTitle("Underwriting - 1008 - Transmittal Summary");

            if (trackedDocuments.Count > 0)
            {
                trackedDocument = trackedDocuments.GetItemAt(0) as TrackedDocument;
            }
            else
            {
                EncompassApplication.CurrentLoan.Log.TrackedDocuments.Add("Underwriting - 1008 - Transmittal Summary", EncompassApplication.CurrentLoan.Log.MilestoneEvents.LastCompletedEvent.MilestoneName);
                trackedDocuments = EncompassApplication.CurrentLoan.Log.TrackedDocuments.GetDocumentsByTitle("Underwriting - 1008 - Transmittal Summary");
                trackedDocument = trackedDocuments.GetItemAt(0) as TrackedDocument;
            }
        }
    }
}
